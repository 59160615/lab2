module.exports = {
    isUserNameValid: function(username){
        if(username.length<3 || username.length>15){
            return false;
        }
        if(username.toLowerCase()!== username){
            return false;
        }
        return true;

    },
    isAgeValid: function(age){
        if(age == ''){
            return false;
        }
        if(age<=100 && age>=18){
            return true;
        }else{
            return false;
        }
       

        return true;
    },

    isPasswordValid: function(pass){
        if(pass.length<8){
            return false;
        }
       
        
        return true;
    },
    isDateValid: function(dateString){
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString)){
            return false; 
        }
       

        var parts = dateString.split("/");
        var day = parseInt(parts[1], 10);
        var month = parseInt(parts[0], 10);
        var year = parseInt(parts[2], 10);

        if(year < 1970 || year > 2020 || month == 0 || month > 12 ) {
          return false;  
        }
        

        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
       
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)){
           monthLength[1] = 29; 
        }
        

        return day > 0 && day <= monthLength[month-1];


    }
}